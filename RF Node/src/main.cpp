#include <Arduino.h>
#include "ros.h"
#include "std_msgs/UInt8.h"
#include "geometry_msgs/Pose2D.h"
#include <SPI.h>
#include <NRFLite.h>

/**
 * Important constants
 */
#define POSE_INTERVALL 1 * 1000   // Interval in wich Pose is sent to small bot

volatile float x;
volatile float y;
volatile float yaw;
volatile uint8_t activate;

bool LILB_OK = false;

#pragma region Debug

/**
 * ROS Node Handle
 * Needed for communication over rosserial
 */
ros::NodeHandle nh;

/**
 * Helper Funtions to better log text
 * @param debug Text to log
 */
void logdebug(String debug) {
  char buf[160];
  debug.toCharArray(buf, 160);
  nh.logdebug(buf);
}

void loginfo(String info) {
  char buf[160];
  info.toCharArray(buf, 160);
  nh.loginfo(buf);
}
#pragma endregion

#pragma region ROS
/**
 * Subscriber and Callback for having current pose
 */
void odomCB(const geometry_msgs::Pose2D &msg_in) {
  x = msg_in.x;
  y = msg_in.y;
  yaw = msg_in.theta;
}
ros::Subscriber<geometry_msgs::Pose2D> odomSub("/robot/position/pose2d", &odomCB);

void actCB(const std_msgs::UInt8& act_msg) {
  activate = act_msg.data;
}
ros::Subscriber<std_msgs::UInt8> subAct("act", &actCB);

/**
 * Pubisher for adding Points
 */
std_msgs::UInt8 add_msg;
ros::Publisher scoreAddPub("add", &add_msg);
#pragma endregion

#pragma region RF
#define RADIO_ID 38                       // ID of Big Bot
#define DESTINATION_RADIO_ID_LIGHT 104    // ID of lighthouse
#define DESTINATION_RADIO_ID_LILB 86     // ID of little bot
#define PIN_RADIO_CE 3
#define PIN_RADIO_CSN 2

struct ActivatePacket // Any packet up to 32 bytes can be sent.
{
  uint8_t FromRadioId;
  uint8_t TeamID;
};

struct PositionPacket {
  uint8_t FromRadioId;
  float x;
  float y;
  float yaw;
};

struct ScorePacket {
  uint8_t FromRadioId;
  uint8_t addScore;
  uint8_t unique_ID;
};

NRFLite radio;

#pragma endregion

void setup() {
  nh.initNode();

  /**
   * Init all Subscribers and Publishers
   */
  nh.subscribe(odomSub);
  nh.subscribe(subAct);

  nh.advertise(scoreAddPub);

  /**
   * Wait until ROS is connected
   */
  while (!nh.connected())
  {
    nh.spinOnce();
  }
  if (!radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN))
  {
    nh.logfatal("Cannot communicate with radio");
  }
}

long lastPoseSend = 0;
uint8_t lastUID = 0;
uint8_t tries = 0;

void loop() {
  nh.spinOnce();

  if (radio.hasData()) {
    ScorePacket scorePack;
    radio.readData(&scorePack);
    if (lastUID != scorePack.unique_ID && scorePack.FromRadioId == DESTINATION_RADIO_ID_LILB)
    {
      add_msg.data = scorePack.addScore;
      scoreAddPub.publish(&add_msg);
    }
  }

  // Sends the Pose every POSE_INTERVALL ms
  if (millis() - lastPoseSend > POSE_INTERVALL)
  {
    PositionPacket posPack_LILB;
    posPack_LILB.FromRadioId = RADIO_ID;
    posPack_LILB.x = x;
    posPack_LILB.y = y;
    posPack_LILB.yaw = yaw;
    if (radio.send(DESTINATION_RADIO_ID_LILB, &posPack_LILB, sizeof(posPack_LILB)))
    {
      // Success
    }
    
  }

  #pragma region RF_Activation
  if (activate != NULL)
  {
    loginfo(String("Sending ")+ activate +String("..."));

    ActivatePacket radioData_LILB;
    radioData_LILB.TeamID = activate;
    radioData_LILB.FromRadioId = RADIO_ID;

    if (!LILB_OK)
    {
      if (radio.send(DESTINATION_RADIO_ID_LILB, &radioData_LILB, sizeof(radioData_LILB)))
      {
        nh.loginfo("... Success for Little Bot");
        LILB_OK = true;
      }
      else
      {
        nh.logwarn("... Failed for Little Bot");
        tries++;
        if (tries > 15) {
          LILB_OK = true;
        }
      }
    }

    if (LILB_OK)
    {
      activate = NULL;
      LILB_OK = false;
      tries = 0;
    }
  }
  #pragma endregion
  
}