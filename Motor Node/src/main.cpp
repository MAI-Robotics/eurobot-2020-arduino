#include <Arduino.h>
#include "ros.h"
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <Encoder.h>

#define ENCODER_L_A 2
#define ENCODER_L_B 4

#define ENCODER_R_A 3
#define ENCODER_R_B 5

#define ISENSE_R A0
#define ISENSE_L A1
#define ISENSE_A A2

/**
 * ROS Node Handle
 * Needed for communication over rosserial
 */
ros::NodeHandle nh;

#pragma region Debugs
/**
 * Helper Funtions to better log text
 * @param debug Text to log
 */
void logdebug(String debug) {
  char buf[160];
  debug.toCharArray(buf, 160);
  nh.logdebug(buf);
}

void loginfo(String info) {
  char buf[160];
  info.toCharArray(buf, 160);
  nh.loginfo(buf);
}
#pragma endregion

Encoder encL(ENCODER_L_A, ENCODER_L_B);
Encoder encR(ENCODER_R_A, ENCODER_R_B);

/**
 * Define Publisher
 * ----
 * First define Message
 * Then define Publisher
 *  Parameters:
 *    - Topic name
 *    - Message that will be published
*/
std_msgs::Int32 encoderL;
std_msgs::Int32 encoderR;

ros::Publisher encoderLPub("/motors/left/feedback/encoder",   &encoderL);
ros::Publisher encoderRPub("/motors/right/feedback/encoder",  &encoderR);

std_msgs::Float32 currentL;
std_msgs::Float32 currentR;
std_msgs::Float32 currentA;

ros::Publisher currentLPub("/motors/left/feedback/current",   &currentL);
ros::Publisher currentRPub("/motors/right/feedback/current",  &currentR);
ros::Publisher currentAPub("/motors/arm/feedback/current",    &currentA);

void readCurrent() {
  currentL.data = analogRead(ISENSE_L) * (5.0 / pow(2, 10));
  currentLPub.publish(&currentL);

  currentR.data = analogRead(ISENSE_R) * (5.0 / pow(2, 10));
  currentRPub.publish(&currentR);

  currentA.data = analogRead(ISENSE_A) * (5.0 / pow(2, 10));
  currentAPub.publish(&currentA);
}

void readEncoders() {
  encoderL.data = encL.read();
  encoderLPub.publish(&encoderL);

  encoderR.data = encR.read();
  encoderRPub.publish(&encoderR);
  //Serial.println(encL.read());
  //Serial.println(encR.read());
}

void setup() {
  nh.initNode();
  //Serial.begin(57600);
  /**
   * Init all Subscribers and Publishers
   * Advertise each and every Publisher with it's variable name
   */
  //nh.advertise(somePublisher)
  //nh.subscribe(someSubscriber)
  nh.advertise(encoderLPub);
  nh.advertise(encoderRPub);
  nh.advertise(currentLPub);
  nh.advertise(currentRPub);
  nh.advertise(currentAPub);

  /**
   * Wait until ROS is connected
   */
  while (!nh.connected())
  {
    nh.spinOnce();
  }
  //loginfo("Node ready! Begin publishing now!");
}

void loop() {
  nh.spinOnce();
  readCurrent();
  delay(10);
  readEncoders();
  delay(10);
}