#include <Arduino.h>
#include "ros.h"
#include <std_msgs/Int16.h>

// Define Hardware Pins
#define ENABLE_L 4  // Enable Pin for the Left Motor
#define ENABLE_R 7  // Enable Pin for the Right Motor

#define PWM_L_FWD 3 // PWM (analog) Pin for the Left Motor; Turn Forward
#define PWM_L_BWD 5 // PWM (analog) Pin for the Left Motor; Turn Backwards

#define PWM_R_FWD 6 // PWM (analog) Pin for the Right Motor; Turn Forward
#define PWM_R_BWD 9 // PWM (analog) Pin for the Right Motor; Turn Backwards

// When Enable Pins are set to this LL (Logic Level) motorcontrollers accept commands
// When this is inversed the motors don't accept commands
// This Parameter was for debugging
#define ENABLE_LOGIC true

/**
 * ROS Node Handle
 * Needed for communication over rosserial
 */
ros::NodeHandle nh;
#ifdef DEBUG
  #pragma region Debugs
  /**
   * Helper Funtions to better log text
   * @param debug Text to log
   */
  void logdebug(String debug) {
    char buf[160];
    debug.toCharArray(buf, 160);
    nh.logdebug(buf);
  }

  void loginfo(String info) {
    char buf[160];
    info.toCharArray(buf, 160);
    nh.loginfo(buf);
  }
  #pragma endregion
#endif // DEBUG

void motorLeftCB(const std_msgs::Int16& speedCMD) {
  if (speedCMD.data < 0)          // If smaller than 0 (negative) => turn motor backwards
  {
    digitalWrite(ENABLE_L, ENABLE_LOGIC);         // Enable Motor (can accept commands and can move)
    analogWrite(PWM_L_BWD, abs(speedCMD.data));   // Set speed of wheel
    analogWrite(PWM_L_FWD, 0);                    // Important! Set speed of other direction to 0
  } else if (speedCMD.data > 0)   // If greater than 0 (positive) => turn motor forwards
  {
    digitalWrite(ENABLE_L, ENABLE_LOGIC);         // Enable Motor (can accept commands and can move)
    analogWrite(PWM_L_FWD, abs(speedCMD.data));   // Set speed of wheel
    analogWrite(PWM_L_BWD, 0);                    // Important! Set speed of other direction to 0
  } else
  { // If speed is 0 disable Motors and set speed to 0
    digitalWrite(ENABLE_L, !ENABLE_LOGIC);        // Disable motor so it does not move any more
    analogWrite(PWM_L_FWD, 0);                    // Set speed to 0
    analogWrite(PWM_L_BWD, 0);                    // Set speed to 0
  }
}

ros::Subscriber<std_msgs::Int16> motorLeft("/motors/left/control/speed", &motorLeftCB);

void motorRightCB(const std_msgs::Int16& speedCMD) {
  if (speedCMD.data < 0)          // If smaller than 0 (negative) => turn motor backwards
  {
    digitalWrite(ENABLE_R, ENABLE_LOGIC);         // Enable Motor (can accept commands and can move)
    analogWrite(PWM_R_BWD, abs(speedCMD.data));   // Set speed of wheel
    analogWrite(PWM_R_FWD, 0);                    // Important! Set speed of other direction to 0
  } else if (speedCMD.data > 0)   // If greater than 0 (positive) => turn motor forwards
  {
    digitalWrite(ENABLE_R, ENABLE_LOGIC);         // Enable Motor (can accept commands and can move)
    analogWrite(PWM_R_FWD, abs(speedCMD.data));   // Set speed of wheel
    analogWrite(PWM_R_BWD, 0);                    // Important! Set speed of other direction to 0
  } else
  { // If speed is 0 disable Motors and set speed to 0
    digitalWrite(ENABLE_R, !ENABLE_LOGIC);        // Disable motor so it does not move any more
    analogWrite(PWM_R_FWD, 0);                    // Set speed to 0
    analogWrite(PWM_R_BWD, 0);                    // Set speed to 0
  }
}

ros::Subscriber<std_msgs::Int16> motorRight("/motors/right/control/speed", &motorRightCB);

void setup() {
  nh.initNode();

  /**
   * Init all Subscribers and Publishers
   * Advertise each and every Publisher with it's variable name
   */
  nh.subscribe(motorLeft);
  nh.subscribe(motorRight);
  /**
   * Wait until ROS is connected
   */
  while (!nh.connected())
  {
    nh.spinOnce();
  }
  pinMode(ENABLE_L, OUTPUT);
  pinMode(ENABLE_R, OUTPUT);
  pinMode(PWM_L_BWD, OUTPUT);
  pinMode(PWM_L_FWD, OUTPUT);
  pinMode(PWM_R_BWD, OUTPUT);
  pinMode(PWM_R_FWD, OUTPUT);
}

void loop() {
  nh.spinOnce();
}