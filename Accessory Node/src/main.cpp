#include <Arduino.h>
#include "ros.h"
#include "std_msgs/UInt16.h"
#include "std_msgs/Bool.h"
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

/**
 * Important constants
 */

#define SCREEN_WIDTH  128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64  // OLED display height, in pixels

#define PULL_PIN 2        // Pin that the pull lead is connected to (PULLUP)

#define ACTUATOR_RELAY 3  // Pin that connects to the relay to cut power when game is over
#define SECONDARY_RELAY 4 // Pin that connects to the second relay

#pragma region Debug

/**
 * ROS Node Handle
 * Needed for communication over rosserial
 */
ros::NodeHandle nh;

/**
 * Helper Funtions to better log text
 * @param debug Text to log
 */
void logdebug(String debug) {
  char buf[160];
  debug.toCharArray(buf, 160);
  nh.logdebug(buf);
}

void loginfo(String info) {
  char buf[160];
  info.toCharArray(buf, 160);
  nh.loginfo(buf);
}
#pragma endregion

#pragma region Display

/**
 * The display object to control the OLED-display
 */
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire);

bool noDisplay = true;     // Will deactivate all display things

/**
 * Helper function to more easily wite text to the display
 * @param write     the text that should be written on the OLED
 * @param fontSize  Font Size for the written text
 */
void writeToDisplay(String write, int fontSize = 8) {
  if (noDisplay) {return;} // If no display was detected don't even attempt to do something with it

  display.clearDisplay();
  display.setTextSize(fontSize);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println(write);
}
#pragma endregion

#pragma region ROS
/**
 * Subscriber and Callback for displaying the points gained during the match
 */
void scoreCB(const std_msgs::UInt16 &msg_in) {
  loginfo(String("Score is now ") + msg_in.data);
  writeToDisplay(String(msg_in.data));
}
ros::Subscriber<std_msgs::UInt16> scoreSub("point_score", &scoreCB);

/**
 * Subscriber and Callback for deactivating actuators on hard error or end of match
 */
void aachenSafeMode(const std_msgs::Bool &msg_in) {
  digitalWrite(ACTUATOR_RELAY, msg_in.data);
}
ros::Subscriber<std_msgs::Bool> aachenSafeModeSub("/control/hardware/power/actuators_enabled", &aachenSafeMode);

/**
 * Subscriber and Callback for deactivating actuators on hard error or end of match
 */
void secondRelayCB(const std_msgs::Bool &msg_in) {
  digitalWrite(SECONDARY_RELAY, !msg_in.data);
}
ros::Subscriber<std_msgs::Bool> secondRelaySub("/control/hardware/power/secondary_enabled", &secondRelayCB);


/**
 * Pubisher for activation system
 */
std_msgs::Bool pull_msg;
ros::Publisher pullPub("pull_to_start", &pull_msg);

/**
 * Pubishes the current state off the activation system
 */
void pullPinCB() {
  pull_msg.data = digitalRead(PULL_PIN);
  pullPub.publish(&pull_msg);
}
#pragma endregion

void setup() {
  nh.initNode();

  /**
   * Init all Subscribers and Publishers
   */
  nh.subscribe(scoreSub);
  nh.subscribe(aachenSafeModeSub);
  nh.subscribe(secondRelaySub);

  nh.advertise(pullPub);

  /**
   * Initialize Display
   */
    /*if(!display.begin(SSD1306_SWITCHCAPVCC, 0x78)) {  // If an error occurs deactivate display
      //nh.logfatal("SSD1306 allocation failed");
      noDisplay = true;
    }
    if (!noDisplay) {display.display();}
    //delay(2000);
    writeToDisplay(String(000));*/
  

  /**
   * PIN MODES
   */
  pinMode(PULL_PIN, INPUT_PULLUP);
  pinMode(ACTUATOR_RELAY, OUTPUT);
  digitalWrite(ACTUATOR_RELAY, HIGH);
  pinMode(SECONDARY_RELAY, OUTPUT);
  digitalWrite(SECONDARY_RELAY, HIGH);
}

long lastPoseSend = 0;

/**
 * Will only execute ruoghly every 20ms the Pull Pin Publisher to not spam that much
 */
void loop() {
  nh.spinOnce();
  pullPinCB();
  delay(20);
  
}