#include <Arduino.h>
#include "ros.h"
#include <std_msgs/Int16.h>
#include <std_msgs/Bool.h>
#include <Servo.h>

#define ENABLE_MOTOR 4

#define PWM_UP 3
#define PWM_DOWN 5

Servo SERVOS[] = {Servo(),Servo(),Servo(),Servo(),Servo()};

#define CLOSED_ANGLE 0
#define OPEN_ANGLE 180

/**
 * ROS Node Handle
 * Needed for communication over rosserial
 */
ros::NodeHandle nh;

#pragma region Debugs
/**
 * Helper Funtions to better log text
 * @param debug Text to log
 */
void logdebug(String debug) {
  char buf[160];
  debug.toCharArray(buf, 160);
  nh.logdebug(buf);
}

void loginfo(String info) {
  char buf[160];
  info.toCharArray(buf, 160);
  nh.loginfo(buf);
}
#pragma endregion

void motorCB(const std_msgs::Int16& speedCMD) {
  if (speedCMD.data < 0)
  {
    digitalWrite(ENABLE_MOTOR, HIGH);
    analogWrite(PWM_DOWN, abs(speedCMD.data));
  } else if (speedCMD.data > 0)
  {
    digitalWrite(ENABLE_MOTOR, HIGH);
    analogWrite(PWM_UP, abs(speedCMD.data));
  } else
  {
    digitalWrite(ENABLE_MOTOR, LOW);
  }
}

void gripperCB(const std_msgs::Bool &msg, int gripper) {
  SERVOS[gripper].write(msg.data ? OPEN_ANGLE : CLOSED_ANGLE);
}

void gripper0CB(const std_msgs::Bool &msg) {
  gripperCB(msg, 0);
}
void gripper1CB(const std_msgs::Bool &msg) {
  gripperCB(msg, 1);
}
void gripper2CB(const std_msgs::Bool &msg) {
  gripperCB(msg, 2);
}
void gripper3CB(const std_msgs::Bool &msg) {
  gripperCB(msg, 3);
}
void gripper4CB(const std_msgs::Bool &msg) {
  gripperCB(msg, 4);
}

ros::Subscriber<std_msgs::Int16> motorSpeed("/motors/arm/control/speed", &motorCB);
ros::Subscriber<std_msgs::Bool> gripper0State("/servos/arm/gripper0/state", &gripper0CB);
ros::Subscriber<std_msgs::Bool> gripper1State("/servos/arm/gripper1/state", &gripper1CB);
ros::Subscriber<std_msgs::Bool> gripper2State("/servos/arm/gripper2/state", &gripper2CB);
ros::Subscriber<std_msgs::Bool> gripper3State("/servos/arm/gripper3/state", &gripper3CB);
ros::Subscriber<std_msgs::Bool> gripper4State("/servos/arm/gripper4/state", &gripper4CB);

void setup() {
  nh.initNode();

  /**
   * Init all Subscribers and Publishers
   * Advertise each and every Publisher with it's variable name
   */
  //nh.advertise(somePublisher)
  //nh.subscribe(someSubscriber)
  nh.subscribe(motorSpeed);
  nh.subscribe(gripper0State);
  nh.subscribe(gripper1State);
  nh.subscribe(gripper2State);
  nh.subscribe(gripper3State);
  nh.subscribe(gripper4State);
  /**
   * Wait until ROS is connected
   */
  while (!nh.connected())
  {
    nh.spinOnce();
  }

  for(int i = 0; i < 5; i++)
  {
    SERVOS[i].attach(6+i);
  }
}

void loop() {
  nh.spinOnce();
}